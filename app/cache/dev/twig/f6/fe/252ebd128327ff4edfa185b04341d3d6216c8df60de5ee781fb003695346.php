<?php

/* IntecBlogBundle:Default:index.html.twig */
class __TwigTemplate_f6fe252ebd128327ff4edfa185b04341d3d6216c8df60de5ee781fb003695346 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 2
            echo "    <div class=\"flash-notice\">
        ";
            // line 3
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "<h1>Blogs</h1>
<ul>
\t";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["blogs"]) ? $context["blogs"] : $this->getContext($context, "blogs")));
        foreach ($context['_seq'] as $context["_key"] => $context["blogItem"]) {
            // line 9
            echo "\t\t<li><a href=\"/blog/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["blogItem"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["blogItem"], "title", array()), "html", null, true);
            echo "</a> posted by <i style=\"color:green\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["blogItem"], "author", array()), "html", null, true);
            echo "</i> on <i style=\"color:deeppink\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["blogItem"], "postedON", array()), "html", null, true);
            echo "</i>
\t\t\t<ul>
\t\t\t\t<li style=\"color:red\">";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["blogItem"], "subTitle", array()), "html", null, true);
            echo "</li>
\t\t\t</ul>
\t\t</li>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['blogItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "IntecBlogBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 15,  55 => 11,  43 => 9,  39 => 8,  35 => 6,  26 => 3,  23 => 2,  19 => 1,);
    }
}
